# includes
BOILERPLATE_FSPATH=./boilerplate

include $(BOILERPLATE_FSPATH)/core/help.mk
include $(BOILERPLATE_FSPATH)/core/ssh.mk
include $(BOILERPLATE_FSPATH)/core/gitr.mk

# overrides
override GITR_SERVER = 			codeberg.org
override GITR_ORG_UPSTREAM = 	amplifycms


# variables
SOURCE_ROOT_FSPATH=			$(PWD)/tmp
## lift
SOURCE_LYFT_REPO_URL=		https://github.com/lyft/boilerplate
SOURCE_LYFT_FSPATH=			$(SOURCE_ROOT_FSPATH)/boilerplate/boilerplate/lyft
TARGET_LYFT_FSPATH=			$(PWD)/boilerplate/lyft
## other....


## print
print:
	@echo
	@echo "-- TOOL: Update remotes --"

	@echo
	@echo SOURCE_ROOT_FSPATH: 		$(SOURCE_ROOT_FSPATH)

	@echo
	@echo -- LYFT
	@echo SOURCE_LYFT_REPO_URL: 	$(SOURCE_LYFT_REPO_URL)
	@echo SOURCE_LYFT_FSPATH: 		$(SOURCE_LYFT_FSPATH)
	@echo TARGET_LYFT_FSPATH: 		$(TARGET_LYFT_FSPATH)
	@echo

all: stage commit
	# runs everything 

	# If not sure run stage first and then commit


## Stages updates
this-stage: this-stage-clean
	mkdir -p $(SOURCE_ROOT_FSPATH)

	# lyft specific
	cd tmp && git clone $(SOURCE_LYFT_REPO_URL)
	cd $(SOURCE_LYFT_FSPATH) && ls -al

## Cleans stages 
this-stage-clean:
	rm -rf $(SOURCE_ROOT_FSPATH)

## Commit
this-commit: this-commit-clean
	# copies files from staging into the actual boilerplate
	cp -r $(SOURCE_LYFT_FSPATH) $(TARGET_LYFT_FSPATH)

## Clean out commit folders
this-commit-clean:
	# deletes the folders in boilerplate to ensure we leave nothing left over.
	rm -rf $(TARGET_LYFT_FSPATH)



## SSH - Create (Creates an example ssh identity)
this-ssh-create:
	$(MAKE) ssh-create-withargs SSH_NAME=foe.bar@example.com

## SSH - list (Lists ssh keys)
this-ssh-list:
	$(MAKE) ssh-list

